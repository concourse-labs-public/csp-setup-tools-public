locals {
  #  iam_perms = sort(flatten(regexall("\"(.*)\"", file("./iam_perms.txt"))))
  built_in_policies = [
    "arn:aws:iam::aws:policy/job-function/ViewOnlyAccess",
    "arn:aws:iam::aws:policy/SecurityAudit"
  ]
}


resource "aws_iam_policy" "concourse_addon_policy1" {
  name        = "ConcourseReadOnlyAddonPolicy1"
  path        = "/"
  description = "Addition Readonly permissions for Concourse Labs"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = local.iam_perms
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_role" "concourse_describe_role1" {
  name = "ConcourseDescribeRole"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = [var.concourse_account_id]

        }
        Condition = {
          test     = "ForAnyValue:StringEquals"
          variable = "sts:ExternalId"
          values   = [var.external_id]
        }
      }
    ]
  })
  managed_policy_arns = concat(local.built_in_policies, [
    aws_iam_policy.concourse_addon_policy1.arn
  ])
}
