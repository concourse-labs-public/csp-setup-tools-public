variable "concourse_account_id" {
  type        = number
  default     = "913274952529"
  description = "Account id for Concourse: DO NOT change unless instructed by Concourse Team"
}

variable "external_id" {
  type        = string
  description = "The value is provided by concourse team: do not change this"
}
