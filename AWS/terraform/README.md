# terraform module Scripts to create access for Concourse Labs ingestors
## Contents
This directory contains the terraform module to create the required permissions

## Pre-Requistes

You will need to know the external Id of the Concourse Labs Tenant/instituion. This is the value of UUID which can be found under Setting -> Institution in the Overview Tab
## How-To
In your terraform project use the module like this:

`
module "concourse_labs" {

  source      = "git::https://gitlab.com/concourse-labs-public/csp-setup-tools-public.git//AWS/terraform"
  external_id = "2ed47c27-4538-460a-be8d-9964a9770902"

}
`