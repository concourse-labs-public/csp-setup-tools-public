# Cloudformation Scripts to create access for Concourse Labs ingestors
## Contents
This directory contains 2 formats of the Cloudformation setup scripts. 
json format
yaml format
Both these files are the same content. You can use either to create the cloudformation Stack
## Pre-Requistes

You will need to know the external Id of the Concourse Labs Tenant/instituion. This is the value of UUID which can be found under Setting -> Institution in the Overview Tab
## How-To
Download the CFT script and create the CFT stack detailed instructions can be found  [here](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/stacks.html)
